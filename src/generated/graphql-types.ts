import { gql } from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

/** Authentication mutation, deletes the session. */
export type LogoutUser = {
  __typename?: 'LogoutUser';
  success?: Maybe<Scalars['Boolean']>;
};

/** Mutation wrapper for all mutations. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Log the user out. */
  logoutUser?: Maybe<LogoutUser>;
};

/** Query wrapper for all queries. */
export type Query = {
  __typename?: 'Query';
  me?: Maybe<User>;
};

/** GraphQL type for the User model. */
export type User = {
  __typename?: 'User';
  email: Scalars['String'];
  /** Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. */
  username: Scalars['String'];
};
