type CardTypes = 'diamond' | 'heart' | 'spade' | 'clover';

interface ICard {
  type: CardTypes;
  text: 'A' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | 'J' | 'Q' | 'K';
}

export type { ICard, CardTypes };
