import React from 'react';
import Game from './screens/Game';

const App: React.FC = () => {
  return <Game />;
};

export default App;
