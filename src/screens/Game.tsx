import party from 'party-js';
import { FC, useEffect, useState } from 'react';
import { useShuffle, shuffle } from 'common/hooks';
import { ICard } from 'types/Card';
import { Board, Button, Card } from '../components';
import { deck } from '../data/deck';

const calculateRemaining = (cards: ICard[] | undefined, turn: number) => {
  let remainingCount = 0;
  if (cards?.length) {
    remainingCount = cards.length - (turn + 1) * 5;

    remainingCount = remainingCount < 2 ? 0 : remainingCount;
  }

  return remainingCount.toString();
};

const getIsSucceed = (shuffledArray: ICard[] | undefined) => {
  if (shuffledArray?.length) {
    if (
      shuffledArray[shuffledArray.length - 1].text === 'A' ||
      shuffledArray[shuffledArray.length - 2].text === 'A'
    ) {
      return true;
    }
  }
  return false;
};

const Game: FC = () => {
  const [shuffledArray, setShuffledArray] = useShuffle(deck);
  const [turn, setTurn] = useState(0);
  const lastTurn = turn === Math.floor(Number(shuffledArray?.length) / 5);
  const isSucceed = getIsSucceed(shuffledArray);
  const openedArray = shuffledArray?.slice(turn * 5, (turn + 1) * 5);
  const [aceCount, setAceCount] = useState(4);

  const handleDeal = () => {
    if (!lastTurn) {
      setTurn(turn + 1);
    }
  };

  useEffect(() => {
    if (openedArray?.filter((card) => card.text === 'A').length) {
      setAceCount(aceCount - openedArray?.filter((card) => card.text === 'A').length);
    }
  }, [shuffledArray]);

  useEffect(() => {
    if (openedArray?.filter((card) => card.text === 'A').length) {
      setAceCount(aceCount - openedArray?.filter((card) => card.text === 'A').length);
    }
  }, [turn]);

  useEffect(() => {
    const partyElement = document.querySelector('#party') as party.sources.DynamicSourceType;
    if (lastTurn && partyElement && isSucceed) {
      party.confetti(partyElement);
    }
  }, [lastTurn, isSucceed]);

  const handleReset = () => {
    const tempArray = shuffledArray && shuffle(shuffledArray);
    tempArray && setShuffledArray([...tempArray]);
    setTurn(0);
    setAceCount(4);
  };

  return (
    <div className="bg-green flex flex-col">
      <div className="linear-gradient-b pt-6 min-h-screen">
        <div className="flex flex-col items-center justify-center mb-8">
          <div className="mb-4">
            <Board
              isSucceed={lastTurn && isSucceed}
              remainingCount={calculateRemaining(shuffledArray, turn)}
              text="Cards Left"
            />
          </div>
          <div>
            <Board isSucceed={false} remainingCount={aceCount.toString()} text="Aces Left" />
          </div>
        </div>
        <div id="party" className="flex-grow">
          <div className="flex justify-center items-center flex-wrap">
            {openedArray?.map((item) => (
              <Card key={`${item.text}-${item.type}`} type={item.type} text={item.text} />
            ))}
          </div>
          {lastTurn && !isSucceed ? (
            <p className="text-center text-2xl text-white mt-14">
              You lose.
              <br /> Better luck next time!
            </p>
          ) : null}
        </div>
        <div className="pt-16 mb-12 flex flex-col justify-center items-center">
          {!lastTurn && (
            <div className="mb-10">
              <Button aria-label="DEAL" type="primary" label="DEAL" onClick={handleDeal} />
            </div>
          )}
          <Button
            type="secondary"
            aria-label={lastTurn ? 'Play Again' : 'Reset'}
            label={lastTurn ? 'Play Again' : 'Reset'}
            onClick={handleReset}
          />
        </div>
      </div>
    </div>
  );
};

export default Game;
