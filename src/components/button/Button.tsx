import React, { FC } from 'react';

interface ButtonProps {
  label: string;
  onClick(): void;
  type: 'primary' | 'secondary';
}

const Button: FC<ButtonProps> = ({ label, type, onClick }) => {
  return (
    <button
      onClick={onClick}
      type="button"
      style={{ fontFamily: 'Alfa Slab One' }}
      className={type === 'primary' ? 'btn-primary' : 'btn-secondary'}
    >
      {label}
    </button>
  );
};

export default Button;
