import React, { FC } from 'react';
import { CardTypes, ICard } from 'types/Card';
import Clover from '../../assets/Clover.svg';
import Diamond from '../../assets/Diamond.svg';
import Heart from '../../assets/Heart.svg';
import Spade from '../../assets/Spade.svg';

const CardImage = (type: CardTypes) => {
  switch (type) {
    case 'clover':
      return Clover;
    case 'spade':
      return Spade;
    case 'heart':
      return Heart;
    case 'diamond':
      return Diamond;
    default:
      return Spade;
  }
};

const handleCardColor = (cardType: CardTypes) => {
  if (cardType === 'clover' || cardType === 'spade') {
    return 'text-black';
  }
  if (cardType === 'heart' || cardType === 'diamond') {
    return 'text-red';
  }
  return '';
};

const Card: FC<ICard> = ({ type, text }) => (
  <div className="h-36 w-28 bg-white m-3 rounded-lg">
    <div className="pt-4 pl-3 ">
      <span className={`text-4xl font-bold ${handleCardColor(type)}`}>{text}</span>
    </div>
    <img className="w-5 ml-4" src={CardImage(type)} alt={`${type} of card`} />
    <img className="w-12 ml-10" src={CardImage(type)} alt={`${type} of card`} />
  </div>
);

export default Card;
