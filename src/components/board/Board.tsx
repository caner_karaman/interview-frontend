import React, { FC } from 'react';
import Winner from '../../assets/winner.svg';

interface BoardProps {
  remainingCount: string;
  text: string;
  isSucceed: boolean;
}

const Board: FC<BoardProps> = ({ remainingCount, isSucceed, text }) => {
  return (
    <div className="relative bg-black border border-yellow-light w-44 h-28 text-center flex flex-col justify-center items-center">
      <h2 className="text-white text-5xl font-bold">{remainingCount}</h2>
      <p className="text-white">{text}</p>
      {isSucceed && (
        <img className="absolute -bottom-4 max-w-none w-96" src={Winner} alt="winner" />
      )}
    </div>
  );
};

export default Board;
