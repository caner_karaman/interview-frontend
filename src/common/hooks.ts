import { Dispatch, SetStateAction, useEffect, useState } from 'react';

function shuffle<T>(array: Array<T>): Array<T> {
  let currentIndex = array.length;
  let randomIndex = 0;
  const tempArray = array;

  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    [tempArray[currentIndex], tempArray[randomIndex]] = [
      tempArray[randomIndex],
      tempArray[currentIndex],
    ];
  }

  return array;
}

function useShuffle<T>(
  array: Array<T>
): [Array<T> | undefined, Dispatch<SetStateAction<T[] | undefined>>] {
  const [shuffledArray, setShuffledArray] = useState<Array<T> | undefined>();

  useEffect(() => {
    const tempArray = shuffle(array);
    setShuffledArray(tempArray);
  }, [array]);

  return [shuffledArray, setShuffledArray];
}

export { useShuffle, shuffle };
