module.exports = {
  content: ['./src/**/*.{html,js,tsx,ts}'],
  theme: {
    colors: {
      black: '#000',
      white: '#FFF',
      green: {
        DEFAULT: '#0A863E',
      },
      yellow: {
        light: '#FFF48C',
        DEFAULT: '#EFCE4B',
      },
      red: '#F64242',
    },
    extend: {},
  },
  plugins: [],
};
